package ddw;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

public class Phrase {
	private static final long wordCoverage = 20;

	private String name;
	private Long startOffset;
	private Long endOffset;
	private int order;
	private double weight;
	private double covWeight;
	private int count;
	private double avgDistance;
	private double coverage;
	private double density;

	public Phrase(String name, Long startOffset, Long endOffset, double weight, int order) {
		this.avgDistance = 0;
		this.name = name;
		this.startOffset = startOffset;
		this.endOffset = endOffset;
		this.weight = weight;
		this.density = 0;
		this.count = 1;
		this.order = order;
		coverage = 2 * Phrase.wordCoverage;
	}

	public void accumulate(Phrase phrase) {
		this.weight += phrase.weight;

		int distance = phrase.order - this.order;
		this.avgDistance = (this.avgDistance + distance) / (1 + 1 * ((this.avgDistance == 0) ? 0 : 1)); //divide by 2 if avgDistance in not 0
		this.order = phrase.order;
		this.count++;
		
		this.coverage += 2 * Phrase.wordCoverage;
		if (distance < (2 * Phrase.wordCoverage))
			this.coverage -= (2 * Phrase.wordCoverage - distance);
	}
	
	public ArrayList<String> getTokenCombinations(int cmpLength) {
		StringTokenizer st = new StringTokenizer(this.name.toLowerCase());
		ArrayList<String> tokens = new ArrayList<String>();

		while (st.hasMoreTokens()) {
			tokens.add(st.nextToken());
		}

		//get all combinations of certain length
		int initTokensNum = tokens.size();
		for (int i = 0; i < initTokensNum - (cmpLength - 1); i++) {
			String cmpToken = tokens.get(i);
			for (int j = i + 1; j < (i + 1) + cmpLength; j++) {
				cmpToken += " " + tokens.get(j);
				tokens.add(cmpToken);
			}
		}

		return tokens;
	}

	public String getName() {
		return this.name;
	}

	public Long getStartOffset() {
		return this.startOffset;
	}

	public Long getEndOffset() {
		return this.endOffset;
	}

	public double getWeight() {
		return this.weight;
	}

	public int getCount() {
		return this.count;
	}

	public double getAvgDistance() {
		return this.avgDistance;
	}

	public double getOrder() {
		return this.order;
	}
	
	public double getCoverage() {
		return this.coverage;
	}
	
	public double getDensity() {
		return this.density;
	}
	
	public double getCovWeight() {
		return this.covWeight;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStartOffset(Long startOffset) {
		this.startOffset = startOffset;
	}

	public void setEndOffset(Long endOffset) {
		this.endOffset = endOffset;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setAvgDistance(double avgDistance) {
		this.avgDistance = avgDistance;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public void setCoverage(double coverage) {
		this.coverage = coverage;
	}
	
	public void setDensity(double density) {
		this.density = density;
	}
	
	public void setCovWeight(double covWeight) {
		this.covWeight = covWeight;
	}

}


class WeightComparator implements Comparator<Phrase> {
    @Override
    public int compare(Phrase a, Phrase b) {
        return a.getWeight() > b.getWeight() ? -1 : a.getWeight() == b.getWeight() ? 0 : 1;
    }
}

