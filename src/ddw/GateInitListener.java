package ddw;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import gate.CreoleRegister;
import gate.Gate;
import gate.util.GateException;

public class GateInitListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

	@Override
	public void contextInitialized(ServletContextEvent e) {
		org.apache.log4j.BasicConfigurator.configure();
		ServletContext ctx = e.getServletContext();

		File gateHomeFile = new File(ctx.getRealPath("/WEB-INF/GATE"));
		Gate.setGateHome(gateHomeFile);
		File pluginsHome = new File(ctx.getRealPath("/WEB-INF/GATE/plugins"));
		Gate.setPluginsHome(pluginsHome);
		Gate.setUserConfigFile(new File("/WEB-INF/GATE", "user.xml"));

		try {
			Gate.init();
		} catch (GateException e1) {
			e1.printStackTrace();
		}

		//load plugins
		CreoleRegister register = Gate.getCreoleRegister();
		URL annieHome;
		try {
			annieHome = new File(pluginsHome, "ANNIE").toURL();
			register.registerDirectories(annieHome);
		} catch (MalformedURLException | GateException e1) {
			e1.printStackTrace();
		}
	}

}
