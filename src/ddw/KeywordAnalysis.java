package ddw;

import java.io.File;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;

import gate.*;
import gate.creole.*;
import gate.util.InvalidOffsetException;

public class KeywordAnalysis {
	private SerialAnalyserController pipeline;
	private Corpus corpus;

	private List<Phrase> oneWordPhrases;
	private List<Phrase> twoWordPhrases;
	private List<Phrase> threeWordPhrases;
	private List<Phrase> phrases;
	private List<Phrase> keyPhrases;

	private int keyphrasesNum;
	private int docLength;

	public KeywordAnalysis(ServletContext ctx, Document document) {
		// create resources and add them to pipeline
		try {
			this.pipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

			ProcessingResource documentResetPR = (ProcessingResource) Factory
					.createResource("gate.creole.annotdelete.AnnotationDeletePR");
			ProcessingResource tokenizerPR = (ProcessingResource) Factory
					.createResource("gate.creole.tokeniser.DefaultTokeniser");
			ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory
					.createResource("gate.creole.splitter.SentenceSplitter");
			ProcessingResource POSTaggerPR = (ProcessingResource) Factory.createResource("gate.creole.POSTagger");

			File keyJape = new File(ctx.getRealPath("/JAPE/keyPhrases.jape"));
			java.net.URI tagsJapeURI = keyJape.toURI();

			FeatureMap keyJapeFeatures = Factory.newFeatureMap();
			keyJapeFeatures.put("grammarURL", tagsJapeURI.toURL());

			keyJapeFeatures.put("encoding", "UTF-8");

			ProcessingResource keyJapePR = (ProcessingResource) Factory.createResource("gate.creole.Transducer",
					keyJapeFeatures);

			pipeline.add(documentResetPR);
			pipeline.add(tokenizerPR);
			pipeline.add(sentenceSplitterPR);
			pipeline.add(POSTaggerPR);
			pipeline.add(keyJapePR);

		} catch (ResourceInstantiationException e1) {
			e1.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		this.phrases = new ArrayList<Phrase>();
		this.oneWordPhrases = new ArrayList<Phrase>();
		this.twoWordPhrases = new ArrayList<Phrase>();
		this.threeWordPhrases = new ArrayList<Phrase>();
		this.keyPhrases = new ArrayList<Phrase>();
		this.docLength = 0;

		// create corpus and document
		try {
			this.corpus = Factory.newCorpus("");
		} catch (ResourceInstantiationException e) {
			e.printStackTrace();
		}

		this.corpus.add(document);
		this.pipeline.setCorpus(this.corpus);
	}

	public int phraseExists(Phrase phrase, List<Phrase> phraseList) {
		// check if phrase exists and return it's index
		for (int i = 0; i < phraseList.size(); i++) {
			if (phraseList.get(i).getName().toLowerCase().equals(phrase.getName().toLowerCase())) {
				return i;
			}
		}
		return -1;
	}

	public void addOrMergePhrase(Phrase phrase) {
		List<Phrase> phraseList = new ArrayList<Phrase>();
		int wordsNum = phrase.getName().split("\\s+").length;

		switch (wordsNum) {
		case 1:
			phraseList = this.oneWordPhrases;
			break;
		case 2:
			phraseList = this.twoWordPhrases;
			break;
		case 3:
			phraseList = this.threeWordPhrases;
			break;
		}

		int index = this.phraseExists(phrase, phraseList);

		if (index >= 0)
			phraseList.get(index).accumulate(phrase);
		else
			phraseList.add(phrase);
	}

	public double getTagWeight(String tagName) {
		double tagWeight = 1;

		switch (tagName.toLowerCase()) {
		case "h1":
			tagWeight = 4;
			break;
		case "h2":
			tagWeight = 3.5;
			break;
		case "h3":
			tagWeight = 3;
			break;
		case "h4":
			tagWeight = 2.5;
			break;
		case "h5":
			tagWeight = 2;
			break;
		case "h6":
			tagWeight = 1.5;
			break;
		case "em":
			tagWeight = 1.3;
			break;
		case "strong":
			tagWeight = 1.5;
			break;
		case "title":
			tagWeight = 4;
			break;
		case "caption":
			tagWeight = 1.5;
			break;
		case "aside":
			tagWeight = 0.75;
			break;
		case "footer":
			tagWeight = 0.75;
			break;
		case "main":
			tagWeight = 1.2;
			break;
		case "a":
			tagWeight = 0;
			break;
		case "nav":
			tagWeight = 0;
			break;
		case "form":
			tagWeight = 0;
			break;
		case "code":
			tagWeight = 0;
			break;
		case "pre":
			tagWeight = 0;
			break;
		case "script":
			tagWeight = 0;
			break;
		case "style":
			tagWeight = 0;
			break;
		case "fieldset":
			tagWeight = 0;
			break;
		case "select":
			tagWeight = 0;
			break;
		case "label":
			tagWeight = 0;
			break;
		case "!":
			tagWeight = 0;
			break;
		}
		;

		return tagWeight;
	}

	public void computeWeights() {
		for (int i = 0; i < this.corpus.size(); i++) {
			Document doc = this.corpus.get(i);
			DocumentContent docCont = doc.getContent();
			AnnotationSet allAnnots = doc.getAnnotations();
			List<Annotation> annotList = gate.Utils.inDocumentOrder(allAnnots);

			AnnotationSet phraseCnt = allAnnots.get("Phrase");
			System.out.println("Number of Phrase annotations:" + phraseCnt.size());

			double weight = 1;
			int nullTags = 0; // number of nested tags that have zero weight
								// (null tags)

			for (int j = 0; j < annotList.size(); j++) {
				Annotation token = (Annotation) annotList.get(j);

				// accumulate weight based on which tags we are currently in
				if (token.getType().equals("OpenTag") || token.getType().equals("CloseTag")) {
					FeatureMap tagFeats = token.getFeatures();
					String tagName = (String) tagFeats.get((Object) "name");

					double tagWeight = this.getTagWeight(tagName);

					// check whether the tag is a "null" tag
					if (tagWeight != 0) {

						// modify weight that is applied to the nested phrases
						if (token.getType().equals("OpenTag"))
							weight *= tagWeight;
						else
							weight /= tagWeight;

					} else {

						// modify number of "null" tags we are currently in
						if (token.getType().equals("OpenTag"))
							nullTags++;
						else
							nullTags = Math.max(nullTags - 1, 0);
					}
				}

				// if we aren't in any "null" tag, push new Phrase object into
				// an array of identified suitable phrases
				else if (nullTags == 0) {
					if (token.getType().equals("Content")) {
						this.docLength++;
					} else if (token.getType().equals("Phrase")) {
						String phraseName = null;
						try {
							phraseName = docCont
									.getContent(token.getStartNode().getOffset(), token.getEndNode().getOffset())
									.toString();
						} catch (InvalidOffsetException e) {
							e.printStackTrace();
						}

						Phrase phrase = new Phrase(phraseName, token.getStartNode().getOffset(),
								token.getEndNode().getOffset(), weight, this.docLength);

						// check if the phrase already exists in the "phrases"
						// array and if it does, merge the values
						this.addOrMergePhrase(phrase);

					}
				} else if (token.getType().equals("Token")) {
					// decrease null tags in the token ends with -- and the next
					// character in document is > (meaning it's a close comment
					// tag)
					FeatureMap tagFeats = token.getFeatures();
					String tokenName = tagFeats.get((Object) "string").toString();
					if (tokenName.length() > 2) {
						if (tokenName.charAt(tokenName.length() - 1) == '-'
								&& tokenName.charAt(tokenName.length() - 2) == '-') {
							nullTags = Math.max(nullTags - 1, 0);
						}
					}
				}
			}
		}
	}

	public double round(double unrounded, int precision, int roundingMode) {
		BigDecimal bd = new BigDecimal(unrounded);
		BigDecimal rounded = bd.setScale(precision, roundingMode);
		return rounded.doubleValue();
	}

	public void adjustValues(List<Phrase> phraseList) {
		for (int i = 0; i < phraseList.size(); i++) {
			Phrase phrase = phraseList.get(i);
			if (phrase.getCount() == 1) {
				phraseList.remove(i--);
				continue;
			}

			// here, the phrase's weight serves as a weighted count
			double weightedDensity = (double) phrase.getWeight() / this.docLength;
			double coverageRatio = (double) phrase.getCoverage() / this.docLength;

			phrase.setDensity(
					this.round(((double) phrase.getCount() / this.docLength) * 100, 2, BigDecimal.ROUND_HALF_UP));

			// increase weight base on text coverage
			phrase.setCovWeight(
					this.round((weightedDensity + weightedDensity * coverageRatio) * 100, 4, BigDecimal.ROUND_HALF_UP));

			// additional rounding
			phrase.setCoverage(this.round(coverageRatio * 100, 2, BigDecimal.ROUND_HALF_UP));
			phrase.setAvgDistance(this.round(phrase.getAvgDistance(), 2, BigDecimal.ROUND_HALF_UP));
		}
	}

	public void mergeWeights() {
		// nothing to merge for one word phrases - add them to final array
		for (int i = 0; i < this.oneWordPhrases.size(); i++) {
			Phrase phrase = this.oneWordPhrases.get(i);
			phrase.setWeight(phrase.getCovWeight());
			this.phrases.add(phrase);
		}

		// add one word phrase weights with associated two word phrase weights
		for (int i = 0; i < this.twoWordPhrases.size(); i++) {
			Phrase phrase = this.twoWordPhrases.get(i);
			List<String> combinations = phrase.getTokenCombinations(1);
			phrase.setWeight(phrase.getCovWeight());

			for (int j = 0; j < this.oneWordPhrases.size(); j++) {
				if (combinations.contains(this.oneWordPhrases.get(j).getName().toLowerCase())) {
					phrase.setWeight(phrase.getWeight() + this.oneWordPhrases.get(j).getCovWeight() * 0.5);
				}
			}
			this.phrases.add(phrase);
		}

		// add one word phrase weights with associated two word phrase weights
		for (int i = 0; i < this.threeWordPhrases.size(); i++) {
			Phrase phrase = this.threeWordPhrases.get(i);
			List<String> combinations = phrase.getTokenCombinations(2);
			phrase.setWeight(phrase.getCovWeight());

			for (int j = 0; j < this.twoWordPhrases.size(); j++) {
				if (combinations.contains(this.twoWordPhrases.get(j).getName().toLowerCase())) {
					phrase.setWeight(phrase.getWeight() + this.twoWordPhrases.get(j).getWeight() * ((double) 2 / 3));
				}
			}
			this.phrases.add(phrase);
		}
	}

	public void analyze() {
		try {
			this.pipeline.execute();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		// compute initial weights based on number of occurences and position
		// within certain tags
		this.computeWeights();

		// adjust weights based on the phrase text coverage and compute
		// additional values
		this.adjustValues(this.oneWordPhrases);
		this.adjustValues(this.twoWordPhrases);
		this.adjustValues(this.threeWordPhrases);

		mergeWeights();

		// sort phrase arrays and leave only 15 items in them (for demonstration
		// purposes)
		Collections.sort(this.oneWordPhrases, new WeightComparator());
		Collections.sort(this.twoWordPhrases, new WeightComparator());
		Collections.sort(this.threeWordPhrases, new WeightComparator());
		Collections.sort(this.phrases, new WeightComparator());

		this.oneWordPhrases.subList(Math.min(15, this.oneWordPhrases.size()), this.oneWordPhrases.size()).clear();
		this.twoWordPhrases.subList(Math.min(15, this.twoWordPhrases.size()), this.twoWordPhrases.size()).clear();
		this.threeWordPhrases.subList(Math.min(15, this.threeWordPhrases.size()), this.threeWordPhrases.size()).clear();
		this.phrases.subList(Math.min(15, this.phrases.size()), this.phrases.size()).clear();

		// select top keyphrases and add them to keyPhrases array
		this.keyphrasesNum = (int) (Math.min(5, this.phrases.size()) + Math.floor(this.docLength / 750));

		for (int i = 0; i < this.keyphrasesNum; i++) {
			this.keyPhrases.add(this.phrases.get(i));
		}
	}

	public Corpus getCorpus() {
		return this.corpus;
	}

	public List<Phrase> getPhrases() {
		return this.phrases;
	}

	public List<Phrase> getOneWordPhrases() {
		return this.oneWordPhrases;
	}

	public List<Phrase> getTwoWordPhrases() {
		return this.twoWordPhrases;
	}

	public List<Phrase> getThreeWordPhrases() {
		return this.threeWordPhrases;
	}

	public List<Phrase> getKeyPhrases() {
		return this.keyPhrases;
	}

	public int getKeyphrasesNum() {
		return this.keyphrasesNum;
	}

	public int getDocLength() {
		return this.docLength;
	}
}
