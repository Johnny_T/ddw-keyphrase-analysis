package ddw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.ProcessingResource;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;*/
import gate.*;
import gate.creole.*;

/**
 * Servlet implementation class Test
 */
@WebServlet("/analyze")
public class GateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//private static KeywordAnalysis app;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GateServlet() {
		super();
	}

	@Override
	public void init() {
		//this.app = new KeywordAnalysis(this.getServletContext());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {		
		Document document = null;

		//load document from URL
		String doc = null;
		if (request.getParameterMap().containsKey("webUrl") && !request.getParameter("webUrl").toString().isEmpty()) {
			URL url = new URL(request.getParameter("webUrl").toString());

			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String inputLine;
			StringBuffer docResponse = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				docResponse.append(inputLine + "\n");
			}
			doc = docResponse.toString();
			in.close();
		} else if (request.getParameterMap().containsKey("webContent") && !request.getParameter("webContent").toString().isEmpty()) {
			doc = request.getParameter("webContent").toString();
		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}

		
		//force markup awareness
		FeatureMap parameters = gate.Factory.newFeatureMap();
		parameters.put(Document.DOCUMENT_ENCODING_PARAMETER_NAME, "UTF-8");
		parameters.put(Document.DOCUMENT_MARKUP_AWARE_PARAMETER_NAME, true);
		parameters.put(Document.DOCUMENT_STRING_CONTENT_PARAMETER_NAME, doc);
		parameters.put(Document.DOCUMENT_MIME_TYPE_PARAMETER_NAME, "text/xml");

		try {
			document = (Document) gate.Factory.createResource("gate.corpora.DocumentImpl", parameters);
		} catch (ResourceInstantiationException e) {
			e.printStackTrace();
		}
		
		//create application with downloaded document
		KeywordAnalysis app = new KeywordAnalysis(this.getServletContext(), document);
		

		//start analysis
		app.analyze();
		
		//direct to JSP
		request.setAttribute("result", app);
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
