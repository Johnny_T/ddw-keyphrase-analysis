<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList, ddw.KeywordAnalysis, ddw.Phrase"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/ddw.css" />
<script src="${pageContext.request.contextPath}/js/Chart.min.js"></script>
<title>Keyword analysis</title>
</head>
<body>
	<header>
	<h1>Keyword analysis</h1>
	<form action="/DDW-DU1/analyze" class="pure-form" method="post">
		<fieldset>
			<input name="webUrl" type="url" placeholder="Enter webpage URL" /><br />
			<textarea name="webContent" rows="4" cols="50"
				placeholder="Or enter webpage content">
			</textarea>
			<br />
			<button type="submit" class="pure-button pure-button-primary">Run
				Analysis</button>
		</fieldset>
	</form>
	</header>

	<c:if test="${result != null}">
		<main> <section>
		<h3>Results</h3>

		<canvas id="weightChart" width="1000" height="500"></canvas>
		
		<table class="pure-table pure-table-horizontal">
			<caption>Merged keyphrase weights</caption>
			<thead>
				<tr>
					<th>Name</th>
					<th>Initial Weight</th>
					<th>Final Weight</th>
				</tr>
			</thead>
			<tbody>				
				<c:set var="count" value="0" scope="page" />
				<c:forEach items="${result.phrases}" var="value">
					<c:set value="" var="topPhrase"></c:set>
					<c:if test="${count < result.keyphrasesNum}">
						<c:set value="keyphrase" var="topPhrase"></c:set>
					</c:if>
					<tr class="${topPhrase}"}>
						<td class="phraseName">${value.name}</td>
						<td class="phraseWeight">${value.covWeight}</td>
						<td class="phraseWeight">${value.weight}</td>
					</tr>
					<c:set var="count" value="${count + 1}" scope="page" />
				</c:forEach>
			</tbody>
		</table>

		<table class="pure-table pure-table-horizontal">
			<caption>Keywords</caption>
			<thead>
				<tr>
					<th>Name</th>
					<th>Count</th>
					<th>Density</th>
					<th>Average Distance (in words)</th>
					<th>Text Coverage</th>
					<th>Weight</th>
				</tr>
			</thead>
			<tbody>
				<c:set var="count" value="0" scope="page" />
				<c:forEach items="${result.oneWordPhrases}" var="value">
					<c:set value="" var="topPhrase"></c:set>
					<c:if test="${count < result.keyphrasesNum}">
						<c:set value="keyphrase" var="topPhrase"></c:set>
					</c:if>
					<c:set value="" var="spam"></c:set>
					<c:if test="${value.density > 2.0}">
						<c:set value="possibleSpam" var="spam"></c:set>
					</c:if>
					<tr class="${topPhrase}"}>
						<td class="phraseName">${value.name}</td>
						<td class="phraseCnt">${value.count}</td>
						<td class="phraseCnt ${spam}">${value.density}%</td>
						<td class="phraseDist">${value.avgDistance}</td>
						<td class="phraseCov">${value.coverage}%</td>
						<td class="phraseWeight">${value.covWeight}</td>
					</tr>
					<c:set var="count" value="${count + 1}" scope="page" />
				</c:forEach>
			</tbody>
		</table>
		
		<table class="pure-table pure-table-horizontal">
			<caption>2-word Keyphrases</caption>
			<thead>
				<tr>
					<th>Name</th>
					<th>Count</th>
					<th>Density</th>
					<th>Average Distance (in words)</th>
					<th>Text Coverage</th>
					<th>Weight</th>
				</tr>
			</thead>
			<tbody>				
			<c:set var="count" value="0" scope="page" />
				<c:forEach items="${result.twoWordPhrases}" var="value">
					<c:set value="" var="topPhrase"></c:set>
					<c:if test="${count < result.keyphrasesNum}">
						<c:set value="keyphrase" var="topPhrase"></c:set>
					</c:if>
					<c:set value="" var="spam"></c:set>
					<c:if test="${value.density > 2.0}">
						<c:set value="possibleSpam" var="spam"></c:set>
					</c:if>
					<tr class="${topPhrase}"}>
						<td class="phraseName">${value.name}</td>
						<td class="phraseCnt">${value.count}</td>
						<td class="phraseCnt ${spam}">${value.density}%</td>
						<td class="phraseDist">${value.avgDistance}</td>
						<td class="phraseCov">${value.coverage}%</td>
						<td class="phraseWeight">${value.covWeight}</td>
					</tr>
					<c:set var="count" value="${count + 1}" scope="page" />
				</c:forEach>
			</tbody>
		</table>
		
		<table class="pure-table pure-table-horizontal">
			<caption>3-word Keyphrases</caption>
			<thead>
				<tr>
					<th>Name</th>
					<th>Count</th>
					<th>Density</th>
					<th>Average Distance (in words)</th>
					<th>Text Coverage</th>
					<th>Weight</th>
					<th>Final Weight</th>
				</tr>
			</thead>
			<tbody>				
				<c:set var="count" value="0" scope="page" />
				<c:forEach items="${result.threeWordPhrases}" var="value">
					<c:set value="" var="topPhrase"></c:set>
					<c:if test="${count < result.keyphrasesNum}">
						<c:set value="keyphrase" var="topPhrase"></c:set>
					</c:if>
					<c:set value="" var="spam"></c:set>
					<c:if test="${value.density > 2.0}">
						<c:set value="possibleSpam" var="spam"></c:set>
					</c:if>
					<tr class="${topPhrase}"}>
						<td class="phraseName">${value.name}</td>
						<td class="phraseCnt">${value.count}</td>
						<td class="phraseCnt ${spam}">${value.density}%</td>
						<td class="phraseDist">${value.avgDistance}</td>
						<td class="phraseCov">${value.coverage}%</td>
						<td class="phraseWeight">${value.covWeight}</td>
						<td class="phraseWeight">${value.weight}</td>
					</tr>
					<c:set var="count" value="${count + 1}" scope="page" />
				</c:forEach>
			</tbody>
		</table>
		</section> </main>

		<script>
			window.onload = function() {
				var keyLabels = [];
				var covData = [];
				var weightData = [];

				<c:forEach items="${result.keyPhrases}" var="value">
				keyLabels.push("${value.name}");
				covData.push("${value.covWeight}");
				weightData.push("${value.weight}");
				</c:forEach>

				var ctx = document.getElementById("weightChart").getContext(
						"2d");
				//var myNewChart = new Chart(ctx).PolarArea(data);

				var data = {
					labels : keyLabels,
					datasets : [{
						label : "Weight",
						fillColor : "rgba(200, 200, 200,0.5)",
						strokeColor : "rgba(200, 200, 200,0.8)",
						highlightFill : "rgba(200, 200, 200,0.75)",
						highlightStroke : "rgba(200, 200, 200,1)",
						data : covData
					},{
						label : "Weight",
						fillColor : "rgba(100, 200, 100,0.5)",
						strokeColor : "rgba(100, 200, 100,0.8)",
						highlightFill : "rgba(100, 200, 100,0.75)",
						highlightStroke : "rgba(100, 200, 100,1)",
						data : weightData
					}]
				};

				Chart.defaults.global.responsive = true;

				var myBarChart = new Chart(ctx).Bar(data);
			};
		</script>
	</c:if>

</body>
</html>